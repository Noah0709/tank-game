﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public List <GameObject> tankOptions;
    public int initialTankNo;
    private GameObject currentTank;
    public float rotateSpeed;
    public Text tankNameText;

	// Use this for initialization
	void Start () {
        initialTankNo = tankOptions.Count;
        nextTank(true);
        rotateSpeed = 60.0f;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("Horizontal"))
        {
            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                nextTank(true);
            }
            else
                nextTank(false);
        }

        if (currentTank != null)
        {
            currentTank.transform.Rotate(Vector3.up * Time.deltaTime * rotateSpeed);
        }
    }

	private void DeativateAllTanks()
	{
		foreach(GameObject tank in tankOptions)
		{
			tank.gameObject.SetActive (false);
		}
	}

    private void SetCurrentTank(int currentTankNo)
    {
        Quaternion tempRotation = new Quaternion();

        if (currentTank != null)
        {
           tempRotation = currentTank.transform.rotation;
        }
        DeativateAllTanks();
        tankOptions[currentTankNo].SetActive(true);
        currentTank = tankOptions[currentTankNo];
        if (currentTank != null)
        {
            currentTank.transform.rotation = tempRotation;
        }
        tankNameText.text = currentTank.name;
    }

    public void nextTank(bool increase)
    {
        if (increase)
        {
            initialTankNo++;

            if (initialTankNo >= tankOptions.Count)
            {
                initialTankNo = 0;
            }
        }
        else
        {
            initialTankNo--;

            if (initialTankNo < 0)
            {
                initialTankNo = tankOptions.Count - 1;
            }
        }

            switch (initialTankNo)
        {
            case 0:
                SetCurrentTank(initialTankNo);
                break;

            case 1:
                SetCurrentTank(initialTankNo);
                break;

            case 2:
                SetCurrentTank(initialTankNo);
                break;

            case 3:
                SetCurrentTank(initialTankNo);
                break;

            case 4:
                SetCurrentTank(initialTankNo);
                break;

            case 5:
                SetCurrentTank(initialTankNo);
                break;
        }
    }
}
